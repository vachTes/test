import { Form } from 'react-bootstrap';
import cls from './index.module.scss'

const Input = ({
  dataItems,
  select,
  required,
  title,
  nameItem,
  onChange,
  value,
  checkSubmit
}) => {
  return (

    <Form.Group controlId={dataItems?.Address8Id} className={cls.inputRow}>
      <div className={cls.inputRowItemTitle}>
        <Form.Label>{title}</Form.Label>
        {
          required
            ?
            (
              <span className={cls.required}>*</span>
            )
            :
            null
        }
      </div>
      <div className={select ? cls.wrapperInputSelect : cls.wrapperInputText}>
        <span />
        {
          select
            ?
            (<>
              <Form.Control
                as='select'
                custom
                onChange={event => onChange(event.target.value)}
                value={value}
                className={cls.inputRowItemField}
              >
                <option disabled value={null} ></option>
                {
                  dataItems?.map((item) => <option value={item[`${nameItem}Id`]} key={item[`${nameItem}Id`]}>{item[`${nameItem}Name`]}</option>
                  )

                }
              </Form.Control>
              {checkSubmit[nameItem] && <div style={{ fontSize: 15, color: 'red' }}>Обязательное поле</div>}
            </>
            )
            :
            (
              <Form.Control
                type='text'
                className={cls.inputRowItemField}
              />
            )
        }
      </div>
    </Form.Group>
  )
}

export { Input }