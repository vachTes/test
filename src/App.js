import { Col, Container, Row, Button } from 'react-bootstrap'
import './App.scss'
import { Input } from './components'
import { useEffect, useState } from 'react'
import getApi from './helpers/fetchAdressData'

function App() {
  const [arrCountry, setArrCountry] = useState([]);
  const [arrRegion, setArrRegion] = useState([]);
  const [arrDistrict, setArrDistrict] = useState([]);
  const [arrLocality, setArrLocality] = useState([]);
  const [arrStreet, setArrStreet] = useState([]);
  /////
  const [valueCountry, setValueCountry] = useState('');
  const [valueRegion, setValueRegion] = useState('');
  const [valueDistrict, setValueDistrict] = useState('');
  const [valueLocality, setValueLocality] = useState('');
  const [valueStreet, setValueStreet] = useState('');

  useEffect(() => {
    getApi(setArrCountry, 'Addresses.Countries', '767659F1-AB94-4E7B-9112-FC2780E03882')
  }, []);
  useEffect(() => {
    if (arrCountry?.length === 1 && !valueCountry) {
      setValueCountry(arrCountry[0].Address8Id)
      console.log(111, valueCountry)
    }
    if (valueCountry) {
      getApi(setArrRegion, 'Addresses.Regions', '767659F1-AB94-4E7B-9112-FC2780E03882',
        {
          "Address8Id": valueCountry
        })
    }
  }, [valueCountry, arrCountry]);
  useEffect(() => {
    if (arrRegion?.length === 1 && !valueRegion) {
      setValueRegion(arrRegion[0].Address7Id)
    }
    if (valueRegion) {
      getApi(setArrDistrict, 'Addresses.Districts', '767659F1-AB94-4E7B-9112-FC2780E03882',
        {
          "Address7Id": valueRegion
        }
      )
    }
  }, [valueRegion, arrRegion]);
  //////////
  useEffect(() => {
    if (arrDistrict?.length === 1 && !valueDistrict) {
      setValueDistrict(arrDistrict[0].Address6Id)
    }
    if (valueDistrict) {
      getApi(setArrLocality, 'Addresses.Cities', '767659F1-AB94-4E7B-9112-FC2780E03882',
        {
          "Address6Id": valueDistrict
        }
      )
    }
  }, [arrDistrict, valueDistrict]);
  useEffect(() => {
    if (arrLocality?.length === 1 && !valueLocality) {
      setValueLocality(arrLocality[0]?.Address5Id)
    }
    if (valueLocality) {
      getApi(setArrStreet, 'Addresses.Streets', '767659F1-AB94-4E7B-9112-FC2780E03882',
        {
          "Address5Id": valueLocality
        }
      )
    }
  }, [arrLocality, valueLocality]);

  useEffect(() => {
    if (arrStreet?.length === 1 && !valueStreet) {
      setValueStreet(arrStreet[0]?.Address4Id)
    }
  }, [arrStreet, valueStreet])


  const checkDelete = (n) => {
    n > 3 && setValueRegion('');
    n > 3 && setArrRegion([]);
    n > 2 && setValueDistrict('');
    n > 2 && setArrDistrict([]);
    n > 1 && setValueLocality('');
    n > 1 && setArrLocality([]);
    n > 0 && setValueStreet('');
    n > 0 && setArrStreet([]);
  }

  const [submit, setSubmit] = useState({
    'Address8': false,
    'Address7': false,
    'Address6': false,
    'Address5': false,
    'Address4': false,
  })
  return (
    <main>
      {
        !arrCountry
          ?
          (
            <h2 style={{display: 'flex', justifyContent: 'center', paddingTop: 50}}>Loading...</h2>
          )
          :
          (
            <>
              <h1 className='title'>евроопт</h1>
              <Container className='wrapper'>
                <Row>
                  <Col>
                    <Input
                      checkSubmit={submit}
                      onChange={el => {
                        setValueCountry(el);
                        checkDelete(4);
                        setSubmit(el => ({
                          ...el, 'Address8': false,
                        }));
                      }}
                      value={valueCountry}
                      dataItems={arrCountry}
                      nameItem="Address8"
                      title='Страна'
                      select
                      required />
                    <Input
                      checkSubmit={submit}
                      onChange={el => {
                        setValueRegion(el);
                        checkDelete(3);
                        setSubmit(el => ({
                          ...el, 'Address7': false,
                        }));
                      }}
                      value={valueRegion}
                      dataItems={arrRegion}
                      nameItem="Address7"
                      title='Область(обл.центр)'
                      select
                      required />
                    <Input
                      checkSubmit={submit}
                      onChange={el => {
                        setValueDistrict(el);
                        checkDelete(2);
                        setSubmit(el => ({
                          ...el, 'Address6': false,
                        }));
                      }}
                      value={valueDistrict}
                      dataItems={arrDistrict}
                      nameItem="Address6"
                      title='Район(район.центр)'
                      select
                      required />
                    <Input
                      checkSubmit={submit}
                      onChange={el => {
                        setValueLocality(el);
                        checkDelete(1);
                        setSubmit(el => ({
                          ...el, 'Address5': false,
                        }));
                      }}
                      value={valueLocality}
                      dataItems={arrLocality}
                      nameItem="Address5"
                      title='Населенный пункт'
                      select
                      required />
                    <Input
                      checkSubmit={submit}
                      onChange={el => {
                        setValueStreet(el)
                        setSubmit(el => ({
                          ...el, 'Address4': false,
                        }));
                      }}
                      value={valueStreet}
                      dataItems={arrStreet}
                      nameItem="Address4"
                      title='Улица'
                      select
                      required />
                    <Input title='Дом' />
                    <Input title='Корпус' />
                    <Input title='Подъезд' />
                    <Input title='Этаж' />
                    <Input title='Квартира' />
                    <Button variant="danger" block size='lg' onClick={() => {
                      setSubmit(_ => ({
                        'Address8': !!!valueCountry,
                        'Address7': !!!valueRegion,
                        'Address6': !!!valueDistrict,
                        'Address5': !!!valueLocality,
                        'Address4': !!!valueStreet,
                      }))
                    }}>
                      Сохранить
            </Button>
                  </Col>
                </Row>
              </Container>
            </>
          )
      }


    </main>
  );
}

export default App;