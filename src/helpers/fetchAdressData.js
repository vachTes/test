const getApi = (callBack, MethodName, ServiceNumber, Data = {}) => {
  fetch('https://rest.eurotorg.by/10201/Json', {
    method: 'POST',
    body: JSON.stringify({
      "CRC": "",
      "Packet": {
        "JWT": "null",
        MethodName,
        ServiceNumber,
        Data
      }
    })
  }).then(resp => resp.json())
    .then(resp => callBack(resp.Table)).catch((e) => alert(e.message))
}
export default getApi